import React from 'react'

import PropTypes from 'prop-types'

import { Typography } from '@lendico/supernova-ds'

import * as S from './styles'

const BoxTitle = ({ title, tag, children, align, ...props }) => (
  <S.Wrapper {...props}>
    {children && <S.BoxImage>{children}</S.BoxImage>}

    {!!title && (
      <Typography tag={tag} align={align} data-id="msg_title_image">
        {title}
      </Typography>
    )}
  </S.Wrapper>
)

BoxTitle.propTypes = {
  title: PropTypes.string,
  tag: PropTypes.string,
  align: PropTypes.string,
  children: PropTypes.node
}

BoxTitle.defaultProps = {
  align: 'center',
  children: null,
  title: '',
  tag: 'h2'
}

export default BoxTitle
