import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    margin-bottom: ${theme.spacings.medium};
    width: 100%;
  `}
`

export const BoxImage = styled.div`
  ${({ theme }) => css`
    align-items: center;
    display: flex;
    justify-content: center;
    margin-bottom: ${theme.spacings.large};

    svg {
      height: 100%;
      max-height: 14rem;
      max-width: 17.5rem;
    }
  `}
`
