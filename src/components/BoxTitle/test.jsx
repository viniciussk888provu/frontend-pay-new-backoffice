import React from 'react'

import { renderWithTheme } from 'utils/tests/helpers'

import { screen } from '@testing-library/react'

import BoxTitle from '.'

const title = 'Defy death'

describe('<BoxTitle />', () => {
  it('should render correctly', () => {
    renderWithTheme(
      <BoxTitle title={title}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          role="img"
          aria-label="Image svg"
          version="1.1"
        >
          <text x="0" y="15">
            SVG
          </text>
        </svg>
      </BoxTitle>
    )

    expect(screen.getByRole('heading', { name: title })).toBeInTheDocument()

    expect(screen.getByRole('img', { name: /Image svg/i })).toBeInTheDocument()
  })
})
