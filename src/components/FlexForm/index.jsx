import styled, { css } from 'styled-components'

const FlexForm = styled.form`
  ${({ theme, spacing }) => css`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    margin-bottom: ${theme.spacings[spacing] || '0px'};
  `}
`

export default FlexForm
