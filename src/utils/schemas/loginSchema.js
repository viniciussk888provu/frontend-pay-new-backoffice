import * as Yup from 'yup'

import { validators } from '../validation/validators'

export const loginSchema = Yup.object().shape({
  user: validators.onlyText,
  password: validators.password
})
