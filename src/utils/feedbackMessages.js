const feedbackMessages = {
  GENERIC_ERROR:
    'Ops! Tivemos um problema inesperado. Por favor, você pode atualizar a página e tentar novamente?'
}

export default feedbackMessages
