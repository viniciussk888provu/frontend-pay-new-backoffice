export const usersList = [
  {
    path: '/backoffice',
    username: 'cdc@backoffice',
    password: 'L3nd1co@backoffice'
  },
  {
    path: '/risco-de-credito',
    username: 'cdc@riscodecredito',
    password: 'L3nd1co@riscodecredito'
  },
  {
    path: '/prevencao-fraude',
    username: 'cdc@prevencaofraude',
    password: 'L3nd1co@prevencaofraude'
  }
]
