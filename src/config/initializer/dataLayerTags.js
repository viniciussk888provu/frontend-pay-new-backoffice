const dataLayerTags = {
  exampleCompleted: {
    event: 'example',
    eventAction: 'exampleCompleted'
  },
  exampleFailed: {
    event: 'example',
    eventAction: 'exampleFailed'
  }
}

export default dataLayerTags
