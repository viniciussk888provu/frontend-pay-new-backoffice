import TagManager from 'react-gtm-module'

const env = process.env.REACT_APP_ENVIRONMENT

export function setDataLayerTags(tags) {
  const tagManager = {
    dataLayer: {
      ...tags
    },
    dataLayerName: 'dataLayer'
  }

  if (env === 'prod' || env === 'sandbox') {
    TagManager.dataLayer(tagManager)
  }
}

export function googleTagManager() {
  let tagManagerArgs
  switch (env) {
    case 'prod': {
      tagManagerArgs = {
        gtmId: 'GTM-N7J7VL4',
        auth: 'SgHA2u7ym5X382RhkFFPSQ',
        preview: 'env-1',
        dataLayer: {
          event: 'setup'
        }
      }

      TagManager.initialize(tagManagerArgs)

      break
    }

    case 'sandbox': {
      tagManagerArgs = {
        gtmId: 'GTM-N7J7VL4',
        auth: '_d-mBp55rWnUKDsFSqKK1w',
        preview: 'env-3',
        dataLayer: {
          event: 'setup'
        }
      }

      TagManager.initialize(tagManagerArgs)

      break
    }

    default: {
      break
    }
  }
}
