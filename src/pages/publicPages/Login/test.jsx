import React from 'react'

import { renderWithReduxRouter } from 'utils/tests/helpers'

import { screen } from '@testing-library/react'

import Login from './index'

describe('<Login />', () => {
  it('should render the heading', () => {
    const { container } = renderWithReduxRouter(<Login />)

    expect(screen.getByRole('heading', { name: /Login/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
