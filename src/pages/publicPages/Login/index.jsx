import React from 'react'

import BoxTitle from 'components/BoxTitle'
import FlexForm from 'components/FlexForm'
import { useFormik } from 'formik'
import { useChangeRoute } from 'hooks'
import { loginSchema } from 'utils/schemas/loginSchema'

import { Input, Button, ActionBox } from '@lendico/supernova-ds'

import { setStorageItem } from '../../../utils/storage'
import { usersList } from '../../../utils/usersList'
import * as S from './styles'

const Login = ({ ...props }) => {
  const initialValues = {
    user: '',
    password: ''
  }

  const handleSubmitForm = (values) => {
    const matchUser = usersList.filter((user) => user.username === values.user)
    matchUser.length === 0 && alert('Usuário ou senha Inválido')
    const matchPassword = matchUser[0].password === values.password
    !matchPassword && alert('Usuário ou senha Inválido')

    if (matchPassword) {
      setStorageItem('@backoffice-user', values.user)
      useChangeRoute(matchUser[0].path)
    }
  }

  const formik = useFormik({
    initialValues,
    validateOnChange: false,
    validateOnBlur: false,
    onSubmit: handleSubmitForm,
    validationSchema: () => loginSchema
  })

  return (
    <S.Wrapper {...props}>
      <BoxTitle title="BackOffice">
        <svg
          width="83"
          height="115"
          viewBox="0 0 83 115"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M68.7714 28.4536H32.0143V4.7679C32.0143 2.15324 34.0667 0 36.6416 0H64.1441C66.6817 0 68.7714 2.11479 68.7714 4.7679V28.4536Z"
            fill="#00FF99"
          />
          <path
            d="M47.3677 113.043C43.8819 115.652 39.0406 115.652 35.5548 113.043L6.66167 87.3788C2.47875 84.2714 0 79.361 0 74.182V28.4536H13.6332V74.182C13.6332 76.1002 14.5628 77.9416 16.112 79.0541L41.5193 102.34L67.1979 79.0541C68.7471 77.9032 69.6766 76.0618 69.6766 74.182V28.4536H83V74.182C83 79.361 80.4825 84.2714 76.3383 87.3788L47.3677 113.043Z"
            fill="#FF4853"
          />
        </svg>
      </BoxTitle>

      <FlexForm onSubmit={formik.handleSubmit} data-id="login_form">
        <Input
          id="user"
          data-id="user"
          label="Usuário"
          type="text"
          name="user"
          value={formik.values.user}
          onChange={formik.handleChange}
          errorMessage={formik.touched.user && formik.errors.user}
        />
        <Input
          id="password"
          data-id="password"
          label="Senha"
          type="password"
          name="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          errorMessage={formik.touched.password && formik.errors.password}
          spacing="xsmall"
        />
        <ActionBox align="center">
          <Button
            disabled={!formik.values.password && !formik.values.user}
            text="Submit"
            type="submit"
            data-id="login_submit"
            id="login_submit"
            onClick={formik.handleSubmit}
          >
            ENTRAR
          </Button>
        </ActionBox>
      </FlexForm>
    </S.Wrapper>
  )
}

export default Login
