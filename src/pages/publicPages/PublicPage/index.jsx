import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'
import { Actions as GenericsExample } from 'redux/ducks/example'

import { Button, ActionBox, Typography } from '@lendico/supernova-ds'

import ImgHero from './hero.png'
import * as S from './styles'

const MainPage = ({ title }) => {
  const dispatch = useDispatch()

  const { name } = useSelector((state) => state.example)

  const handleSubmit = () => {
    dispatch(
      GenericsExample.exampleType({
        name: 'name test'
      })
    )
  }

  return (
    <S.Wrapper>
      <S.Main>
        <Typography tag="h1">{title}</Typography>
        <Typography tag="h3">{name}</Typography>

        <S.Illustration
          src={ImgHero}
          alt="Um desenvolvedor de frente para um notebook."
        />

        <ActionBox uuid="action_box">
          <Button uuid="submit" onClick={handleSubmit}>
            Submit
          </Button>
        </ActionBox>
      </S.Main>
    </S.Wrapper>
  )
}

MainPage.propTypes = {
  title: PropTypes.string
}

MainPage.defaultProps = {
  title: 'React Boilerplate'
}

export default MainPage
