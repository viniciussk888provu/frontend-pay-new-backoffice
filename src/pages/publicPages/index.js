import { lazy } from 'react'

const Pages = {
  PublicPage: lazy(() => import('./PublicPage')),
  Login: lazy(() => import('./Login'))
}

export default Pages
