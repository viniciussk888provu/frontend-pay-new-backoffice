import { lazy } from 'react'

const Pages = {
  PrivatePage: lazy(() => import('./PrivatePage'))
}

export default Pages
