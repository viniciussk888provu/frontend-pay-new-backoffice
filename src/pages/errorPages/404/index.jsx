import React from 'react'

import { listRoutes } from 'routes'
import { isAuthenticated } from 'services/auth'

import { Button, Typography } from '@lendico/supernova-ds'

import * as S from './styles'

const Error404 = () => {
  const handleSwitch = () => {
    if (isAuthenticated()) {
      listRoutes.private.redirect()
    } else {
      listRoutes.public.redirect()
    }
  }

  return (
    <S.Wrapper>
      <Typography align="center" spacing="xlarge">
        Parece que a página que você tentou acessar não existe. Mas, você pode
        voltar para a nossa página principal e prosseguir a partir dela.
      </Typography>

      <Button onClick={handleSwitch} data-id="btn_link_404">
        ok, ir para a página principal
      </Button>
    </S.Wrapper>
  )
}

export default Error404
