const useTitlePage = (title = 'Provu') => {
  document.title = title === 'Provu' ? 'Provu' : `${title} | Provu`
}

export default useTitlePage
