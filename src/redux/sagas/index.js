import { all } from 'redux-saga/effects'

import DataLayerTags from './dataLayerTags'
import Example from './example'

export default function* rootSaga() {
  yield all([DataLayerTags(), Example()])
}
