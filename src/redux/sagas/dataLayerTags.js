import { setDataLayerTags, dataLayerTags } from 'config/initializer'
import { takeEvery, spawn } from 'redux-saga/effects'
import { Types as ExampleTypes } from 'redux/ducks/example'

function* fetchDataLayerTags(action) {
  const { type } = yield action

  switch (type) {
    case ExampleTypes.EXAMPLE_TYPE: {
      yield setDataLayerTags(dataLayerTags.exampleCompleted)
      break
    }

    default:
      break
  }
}

function* watchDataLayerTags() {
  yield takeEvery('*', fetchDataLayerTags)
}

export default function* init() {
  yield spawn(watchDataLayerTags)
}
