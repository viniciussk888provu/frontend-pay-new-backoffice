import React from 'react'

import { routes as Routes } from 'routes'
import { ThemeProvider } from 'styled-components'

import { GlobalStyles, LabelEnv, themeDefault } from '@lendico/supernova-ds'

function App() {
  const env = process.env.REACT_APP_ENVIRONMENT

  return (
    <ThemeProvider theme={themeDefault}>
      <LabelEnv env={env} />

      <GlobalStyles />

      <Routes />
    </ThemeProvider>
  )
}

export default App
