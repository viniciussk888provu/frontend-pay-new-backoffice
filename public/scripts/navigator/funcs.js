var result = bowser.getParser(window.navigator.userAgent)

var browserInfo = {
  nameBrowser: result.parsedResult.browser.name.toLowerCase(),
  versionBrowser: parseInt(result.parsedResult.browser.version),
  nameOS: result.parsedResult.os.name.toLowerCase(),
  versionOS: result.parsedResult.os.versionName,
  platform: result.parsedResult.platform.type,
  userAgent: result._ua
}


function browserPage() {
  var html =
    '<article style="display: block;text-align: left;width: 100%;max-width: 450px;padding: 48px 16px 0;margin: 0 auto;">\n' +
    '    <img src="/logo.svg" alt="logo" style="width: 150px;margin-bottom: 32px;">\n' +
    '    <h1\n' +
    '        style="font-family: Space Grotesk, -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif;font-size: 24px;color: #135464;margin-bottom: 32px; margin-top: 0;">\n' +
    '        Parece que temos um problema técnico aqui.</h1>\n' +
    '    <p\n' +
    '        style="font-family: Source Sans Pro, -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; font-size: 16px;line-height: 1.6;margin-bottom: 16px;">\n' +
    '        Notamos que o seu navegador é incompatível com a tecnologia do nosso site.</p>\n' +
    '    <p\n' +
    '        style="font-family: Source Sans Pro, -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; font-size: 16px;line-height: 1.6;margin-bottom: 32px;">\n' +
    '        O ideal seria você atualizar seu navegador para a versão mais atual.</p>\n' +
    '    <p\n' +
    '        style="font-family: Source Sans Pro, -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; font-size: 16px;line-height: 1.6;margin-bottom: 16px;">\n' +
    '        Caso precise de ajuda, <a style="color: #FF4853;text-decoration: none;"\n' +
    '            href="https://provu.com.br/duvidas/">entre em contato com a gente.</a></p>\n' +
    '</article>'

  document.getElementById('root').style.display = 'none'

  var body = document.getElementsByTagName('body')[0]
  body.style.backgroundColor = '#fff'
  body.insertAdjacentHTML('afterbegin', html)
}

function detectBrowser() {
  if (browserInfo.userAgent.indexOf('Firefox') > -1) {
    browserInfo.nameBrowser = 'Firefox'

    if (browserInfo.versionBrowser < 52) {
      browserPage(browserInfo)
    }
  } else if (browserInfo.userAgent.indexOf('SamsungBrowser') > -1) {
    browserInfo.nameBrowser = 'Samsung Internet'

    if (browserInfo.versionBrowser < 10) {
      browserPage(browserInfo)
    }
  } else if (
    browserInfo.userAgent.indexOf('Opera') > -1 ||
    browserInfo.userAgent.indexOf('OPR') > -1
  ) {
    browserInfo.nameBrowser = 'Opera'

    if (browserInfo.versionBrowser < 44) {
      browserPage(browserInfo)
    }
  } else if (
    browserInfo.userAgent.indexOf('Trident') > -1 ||
    browserInfo.userAgent.indexOf('MSIE') > -1
  ) {
    browserInfo.nameBrowser = 'Microsoft Internet Explorer'

    browserPage(browserInfo)
  } else if (
    browserInfo.userAgent.indexOf('Edge') > -1 ||
    browserInfo.userAgent.indexOf('Edg') > -1
  ) {
    browserInfo.nameBrowser = 'Microsoft Edge'

    if (browserInfo.versionBrowser < 16) {
      browserPage(browserInfo)
    }
  } else if (browserInfo.userAgent.indexOf('Chrome') > -1) {
    browserInfo.nameBrowser = 'Google Chrome'

    if (browserInfo.versionBrowser < 58) {
      browserPage(browserInfo)
    }
  } else if (browserInfo.userAgent.indexOf('Safari') > -1) {
    browserInfo.nameBrowser = 'Safari'

    if (browserInfo.versionBrowser < 10) {
      browserPage(browserInfo)
    }
  } else {
    browserInfo.nameBrowser = 'unknown'
  }
}

detectBrowser()
